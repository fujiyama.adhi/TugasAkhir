package adita.verifikasi;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import static java.lang.Math.abs;

public class CameraMatching extends AppCompatActivity {

    private String filePath = null;
    private ImageView imgPreview, imgRek;
    TextView teks_jarak;
    int min, red1, green1, blue1, red2, green2, blue2, redT, greenT, blueT;
    Bitmap bitmap, rek_bitmap, bmap;
    int [] dists;
    boolean flag;
    String[] r_id, r_latitude, r_longitude, r_nomor, r_teks, r_alamat, r_detail, r_sipr, r_perusahaan, r_jenis, r_ukuran, r_gambar, r_waktu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_matching);
        imgPreview = (ImageView) findViewById(R.id.imgPreview);
        imgRek = (ImageView) findViewById(R.id.imgData);

        terima();
    }

    public void matching(View view){
        if (dists[min] < 50) {
            if (redT < 20 && greenT < 20 && blueT < 20) {
                Intent intent = new Intent(this, InfoReklame.class);

                Bundle c = new Bundle();

                c.putString("id_R", r_id[min]);
                c.putString("latitude", r_latitude[min]);
                c.putString("longitude", r_longitude[min]);
                c.putString("no_F", r_nomor[min]);
                c.putString("teks_RE", r_teks[min]);
                c.putString("alamat_R", r_alamat[min]);
                c.putString("detail_R", r_detail[min]);
                c.putString("sipr_R", r_sipr[min]);
                c.putString("nama_perusahaan", r_perusahaan[min]);
                c.putString("jenis_R", r_jenis[min]);
                c.putString("ukuran", r_ukuran[min]);
                c.putString("gambar", r_gambar[min]);
                c.putString("masa_R", r_waktu[min]);

                intent.putExtras(c);

                startActivity(intent);
            }
            else{
                Toast.makeText(CameraMatching.this, "Gambar Tidak Sama\n" + "Matching Menggunakan Lokasi" , Toast.LENGTH_SHORT).show();
            }

        }
        else {
            Toast.makeText(CameraMatching.this, "Tidak Ada Reklame" , Toast.LENGTH_SHORT).show();
        }
    }

    public void terima (){
        Intent i = getIntent();
        filePath = i.getStringExtra("filePath");
        dists = i.getIntArrayExtra("dists");
        min = i.getIntExtra("min",min);
        r_id = i.getStringArrayExtra("id");
        r_latitude = i.getStringArrayExtra("latitude");
        r_longitude = i.getStringArrayExtra("longitude");
        r_nomor = i.getStringArrayExtra("nomor");
        r_teks = i.getStringArrayExtra("teks");
        r_alamat = i.getStringArrayExtra("alamat");
        r_detail = i.getStringArrayExtra("detail");
        r_sipr = i.getStringArrayExtra("sipr");
        r_perusahaan = i.getStringArrayExtra("perusahaan");
        r_jenis = i.getStringArrayExtra("jenis");
        r_ukuran = i.getStringArrayExtra("ukuran");
        r_gambar = i.getStringArrayExtra("gambar");
        r_waktu = i.getStringArrayExtra("waktu");


        teks_jarak = (TextView)findViewById(R.id.nilai_jarak);
//        teks_jarak.setText(String.valueOf(dists[min]));
        boolean isImage = i.getBooleanExtra("isImage", true);

        if (filePath != null) {
            // Displaying the image or video on the screen
            previewMedia(isImage);
        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
        }
    }

    private void previewMedia(boolean isImage) {
        // Checking whether captured media is image or video
        if (isImage) {
            imgPreview.setVisibility(View.VISIBLE);
            imgRek.setVisibility(View.VISIBLE);

            // bimatp factory
            BitmapFactory.Options options = new BitmapFactory.Options();

            // down sizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 8;

            bitmap = BitmapFactory.decodeFile(filePath, options);
            imgRek.setImageBitmap(bitmap);
            Picasso.with(this).load(r_gambar[min]).into(imgPreview);
        }
    }


    public void rata1 (){
        int redColors = 0;
        int greenColors = 0;
        int blueColors = 0;
        int pixelCount = 0;

        for (int y = 0; y < bitmap.getHeight(); y++)
        {
            for (int x = 0; x < bitmap.getWidth(); x++)
            {
                int c = bitmap.getPixel(x, y);
                pixelCount++;
                redColors += Color.red(c);
                greenColors += Color.green(c);
                blueColors += Color.blue(c);
            }
        }
        // calculate average of bitmap r,g,b values
        red1 = (redColors/pixelCount);
        green1 = (greenColors/pixelCount);
        blue1 = (blueColors/pixelCount);
    }

    public void rata2 (){
        int redColors = 0;
        int greenColors = 0;
        int blueColors = 0;
        int pixelCount = 0;

        for (int y = 0; y < bmap.getHeight(); y++)
        {
            for (int x = 0; x < bmap.getWidth(); x++)
            {
                int c = bmap.getPixel(x, y);
                pixelCount++;
                redColors += Color.red(c);
                greenColors += Color.green(c);
                blueColors += Color.blue(c);
            }
        }
        // calculate average of bitmap r,g,b values
        red2 = (redColors/pixelCount);
        green2 = (greenColors/pixelCount);
        blue2 = (blueColors/pixelCount);
    }

    public void hitung(){
        redT = abs(red1 - red2);
        greenT = abs(green1 - green2);
        blueT = abs(blue1 - blue2);
    }

    public void Test(View view){
//        rek_bitmap = getBitmapFromURL(r_gambar[min]);
        imgPreview.buildDrawingCache();
        bmap = imgPreview.getDrawingCache();


        if (bmap != null){
//            flag = compareImages(bitmap, bmap);
            rata1();
            rata2();
            hitung();
            teks_jarak.setText(String.valueOf(redT) + " " + String.valueOf(greenT) + " " + String.valueOf(blueT));
        }

    }
}
