package adita.verifikasi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UploadKoorActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private ListView mListView;


    private List<HashMap<String, String>> list_data = new ArrayList<>();

    private static final String KEY_ID = "id";
    private static final String KEY_NO = "no_formulr";
    private static final String KEY_TEKS = "teks_reklame";
    private static final String KEY_ALAMAT = "alamat_reklame";
    private static final String KEY_DETAIL = "detail_lokasi";
    private static final String KEY_SIPR = "no_sipr";
    private static final String KEY_PERUSAHAAN = "nama_perusahaan";
    private static final String KEY_JENIS = "jenis";
    private static final String KEY_UKURAN = "ukuran";
    private static final String KEY_GAMBAR = "gambar";


    String[] r_id, r_latitude, r_longitude, r_nomor, r_teks, r_alamat,
            r_detail, r_sipr, r_perusahaan, r_jenis, r_ukuran, r_gambar,
            r_waktu, r_dists;
    double lat, lng;
    LatLng koorbaru;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_koor);

        Bundle b=this.getIntent().getExtras();
        lat =b.getDouble("latitude");
        lng =b.getDouble("longitude");
        r_id =b.getStringArray("id2");
        r_nomor = b.getStringArray("nomor2");
        r_teks = b.getStringArray("teks2");
        r_alamat = b.getStringArray("alamat2");
        r_detail = b.getStringArray("detail2");
        r_sipr = b.getStringArray("sipr2");
        r_perusahaan = b.getStringArray("perusahaan2");
        r_jenis = b.getStringArray("jenis2");
        r_ukuran = b.getStringArray("ukuran2");
        r_gambar = b.getStringArray("gambar2");
        koorbaru = new LatLng(lat, lng);

        conversi();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        mListView = (ListView) findViewById(R.id.list_view);
        mListView.setOnItemClickListener(this);
        onLoaded();
    }

    public void conversi(){

        for (int i=0; i<r_id.length; i++) {
            HashMap<String, String> rek_data = new HashMap<>();
//            rek_data.put(KEY_DISTS,r_dists[i]);
            rek_data.put(KEY_ID, r_id[i]);
            rek_data.put(KEY_NO, r_nomor[i]);
            rek_data.put(KEY_TEKS, r_teks[i]);
            rek_data.put(KEY_ALAMAT, r_alamat[i]);
            rek_data.put(KEY_DETAIL, r_detail[i]);
            rek_data.put(KEY_SIPR, r_sipr[i]);
            rek_data.put(KEY_PERUSAHAAN, r_perusahaan[i]);
            rek_data.put(KEY_JENIS, r_jenis[i]);
            rek_data.put(KEY_UKURAN, r_ukuran[i]);
            rek_data.put(KEY_GAMBAR, r_gambar[i]);


            list_data.add(rek_data);
        }
    }

    public void onLoaded() {
        loadListView();
    }


    public void onError() {

        Toast.makeText(this, "Error !", Toast.LENGTH_SHORT).show();
    }


    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

//        Toast.makeText(this, mAndroidMapList.get(i).get(KEY_TEKS),Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, InfoReklame.class);

        Bundle bundle = new Bundle();
        bundle.putString("id_R", list_data.get(i).get(KEY_ID));
        bundle.putString("no_F", list_data.get(i).get(KEY_NO));
        bundle.putString("teks_RE", list_data.get(i).get(KEY_TEKS));
        bundle.putString("alamat_R", list_data.get(i).get(KEY_ALAMAT));
        bundle.putString("detail_R", list_data.get(i).get(KEY_DETAIL));
        bundle.putString("sipr_R", list_data.get(i).get(KEY_SIPR));
        bundle.putString("nama_perusahaan", list_data.get(i).get(KEY_PERUSAHAAN));
        bundle.putString("jenis_R", list_data.get(i).get(KEY_JENIS));
        bundle.putString("ukuran", list_data.get(i).get(KEY_UKURAN));
        bundle.putString("gambar", list_data.get(i).get(KEY_GAMBAR));
        bundle.putDouble("latitude", lat);
        bundle.putDouble("longitude", lng);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void loadListView() {

        ListAdapter adapter = new SimpleAdapter(UploadKoorActivity.this, list_data, R.layout.list_item,
                new String[] { KEY_TEKS, KEY_NO, KEY_PERUSAHAAN },
                new int[] { R.id.idR,R.id.noR, R.id.teksR });

        mListView.setAdapter(adapter);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

//    @Override
//    public void onBackPressed() {
//        Intent intent = new Intent(this, MapsActivity.class);
//        startActivity(intent);
//    }
}
