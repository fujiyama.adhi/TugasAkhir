package adita.verifikasi.Object;

import android.support.annotation.NonNull;

import java.util.Comparator;

/**
 * Created by adi on 09/04/2017.
 */

public class Reklame implements Comparable<Reklame>{
    private String reklame_id;
    private double reklame_latitude;
    private double reklame_longitude;
    private String reklame_nomor;
    private String reklame_teks;
    private String reklame_alamat;
    private String reklame_detail;
    private String reklame_sipr;
    private String reklame_perusahaan;
    private String reklame_jenis;
    private String reklame_ukuran;
    private String reklame_gambar;
    private String reklame_waktu;
    private String reklame_laporan;
    private Double reklame_jarak;

    public Reklame(){};

    public Reklame(String reklame_id, double reklame_latitude, double reklame_longitude,
                   String reklame_nomor, String reklame_teks, String reklame_alamat,
                   String reklame_detail, String reklame_sipr, String reklame_perusahaan,
                   String reklame_jenis, String reklame_ukuran, String reklame_gambar,
                   String reklame_waktu, String reklame_laporan, Double reklame_jarak) {
        super();
        this.reklame_id = reklame_id;
        this.reklame_latitude = reklame_latitude;
        this.reklame_longitude = reklame_longitude;
        this.reklame_nomor = reklame_nomor;
        this.reklame_teks = reklame_teks;
        this.reklame_alamat = reklame_alamat;
        this.reklame_detail = reklame_detail;
        this.reklame_sipr = reklame_sipr;
        this.reklame_perusahaan = reklame_perusahaan;
        this.reklame_jenis = reklame_jenis;
        this.reklame_ukuran = reklame_ukuran;
        this.reklame_gambar = reklame_gambar;
        this.reklame_waktu = reklame_waktu;
        this.reklame_laporan = reklame_laporan;
        this.reklame_jarak = reklame_jarak;
    }

    public String getReklame_id() {
        return reklame_id;
    }
    public void setReklame_id(String reklame_id) {
        this.reklame_id = reklame_id;
    }
    public double getReklame_latitude() {
        return reklame_latitude;
    }
    public void setReklame_latitude(double reklame_latitude) {
        this.reklame_latitude = reklame_latitude;
    }
    public double getReklame_longitude() {
        return reklame_longitude;
    }
    public void setReklame_longitude(double reklame_longitude) {
        this.reklame_longitude = reklame_longitude;
    }
    public String getReklame_nomor() {
        return reklame_nomor;
    }
    public void setReklame_nomor(String reklame_nomor) {
        this.reklame_nomor = reklame_nomor;
    }
    public String getReklame_teks() {
        return reklame_teks;
    }
    public void setReklame_teks(String reklame_teks) {
        this.reklame_teks = reklame_teks;
    }
    public String getReklame_alamat() {
        return reklame_alamat;
    }
    public void setReklame_alamat(String reklame_alamat) {
        this.reklame_alamat = reklame_alamat;
    }
    public String getReklame_detail() {
        return reklame_detail;
    }
    public void setReklame_detail(String reklame_detail) {
        this.reklame_detail = reklame_detail;
    }
    public String getReklame_sipr() {
        return reklame_sipr;
    }
    public void setReklame_sipr(String reklame_sipr) {
        this.reklame_sipr = reklame_sipr;
    }
    public String getReklame_perusahaan() {
        return reklame_perusahaan;
    }
    public void setReklame_perusahaan(String reklame_perusahaan) {
        this.reklame_perusahaan = reklame_perusahaan;
    }
    public String getReklame_jenis() {
        return reklame_jenis;
    }
    public void setReklame_jenis(String reklame_jenis) {
        this.reklame_jenis = reklame_jenis;
    }
    public String getReklame_ukuran() {
        return reklame_ukuran;
    }
    public void setReklame_ukuran(String reklame_ukuran) {
        this.reklame_ukuran = reklame_ukuran;
    }

    public String getReklame_gambar() {
        return reklame_gambar;
    }

    public void setReklame_gambar(String reklame_gambar) {
        this.reklame_gambar = reklame_gambar;
    }

    public String getReklame_waktu() {
        return reklame_waktu;
    }

    public void setReklame_waktu(String reklame_waktu) {
        this.reklame_waktu = reklame_waktu;
    }

    public String getReklame_laporan() {
        return reklame_laporan;
    }

    public void setReklame_laporan(String reklame_laporan) {
        this.reklame_laporan = reklame_laporan;
    }

    public Double getReklame_jarak() {
        return reklame_jarak;
    }

    public void setReklame_jarak(Double reklame_jarak) {
        this.reklame_jarak = reklame_jarak;
    }


    @Override
    public int compareTo(@NonNull Reklame comparereklame) {
        Double compareJarak = ((Reklame) comparereklame).getReklame_jarak();
        return (int) (this.reklame_jarak - compareJarak);
    }

    public static Comparator<Reklame> ReklameJarakComparator
            = new Comparator<Reklame>() {

        public int compare(Reklame fruit1, Reklame fruit2) {

            Double RekJarak1 = fruit1.getReklame_jarak();
            Double RekJarak2 = fruit2.getReklame_jarak();

            //ascending order
            return RekJarak1.compareTo(RekJarak2);

            //descending order
            //return fruitName2.compareTo(fruitName1);
        }

    };
}
