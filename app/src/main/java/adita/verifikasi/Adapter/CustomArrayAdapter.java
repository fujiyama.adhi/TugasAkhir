package adita.verifikasi.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import adita.verifikasi.Object.Reklame;
import adita.verifikasi.R;

/**
 * Created by adi on 09/04/2017.
 */

public class CustomArrayAdapter extends ArrayAdapter{

    List<Reklame> reklame_List;
    public CustomArrayAdapter(@NonNull Context context, List<Reklame> reklames) {
        super(context, 0, reklames);
        reklame_List = reklames;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        Reklame in = (Reklame) reklame_List.get(position);
        if (convertView == null) {

            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,parent,false);
// inflate custom layout called row
            holder = new ViewHolder();
            holder.teks =(TextView) convertView.findViewById(R.id.teksR);
            holder.id = (TextView) convertView.findViewById(R.id.idR);
            holder.nomor = (TextView) convertView.findViewById(R.id.noR);
            holder.jarak = (TextView) convertView.findViewById(R.id.teksJarak);
            holder.gambar = (ImageView) convertView.findViewById(R.id.gambar_reklame_list);
// initialize textview
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.id.setText(in.getReklame_id());
        holder.nomor.setText(in.getReklame_nomor());
        holder.teks.setText(in.getReklame_teks());
        holder.jarak.setText(String.valueOf(in.getReklame_jarak()) + "m");
        Picasso.with(getContext()).load(in.getReklame_gambar()).into(holder.gambar);
        // set the name to the text;

        return convertView;

    }

    static class ViewHolder
    {
        TextView id;
        TextView teks;
        TextView nomor;
        TextView jarak;
        ImageView gambar;
    }
}
