package adita.verifikasi;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import adita.verifikasi.Config.Config;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
    private static final String TAG = MapsActivity.class.getSimpleName();
    private GoogleMap mMap;
    double lat, lng, latt, lngg, starttime, EndTime;
    JSONArray peoples = null;
    boolean mar = false;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    Marker markerA1, m;
    Marker[] markerfix;
    int merah = 0, orange = 30, hijau = 150, biru = 200, flag =0, min;

    LatLng latLng;
    View I_BE;
    Button b_BE, b_BV, b_SE, b_SV, b_daftar, b_cari, b_baru, b_tambah, b_ganti;
    String getCurrentDateTime, myJSON;
    ArrayList<String> rek_id, rek_latitude, rek_longitude, rek_nomor, rek_teks,
            rek_alamat, rek_detail, rek_sipr, rek_perusahaan, rek_jenis,
            rek_ukuran, rek_gambar, rek_waktu, rek_laporan,
            rek_id2, rek_nomor2, rek_teks2, rek_alamat2, rek_detail2, rek_sipr2,
            rek_perusahaan2, rek_jenis2, rek_ukuran2, rek_gambar2;
    private static final String TAG_RESULTS = "result";
    private static final String TAG_ID = "id";
    private static final String TAG_LAT = "latitude";
    private static final String TAG_LONG = "longitude";
    private static final String TAG_NO = "no_formulr";
    private static final String TAG_TEKS = "teks_reklame";
    private static final String TAG_ALAMAT = "alamat_reklame";
    private static final String TAG_DETAIL = "detail_lokasi";
    private static final String TAG_SIPR = "no_sipr";
    private static final String TAG_PERUSAHAAN = "nama_perusahaan";
    private static final String TAG_JENIS = "jenis";
    private static final String TAG_UKURAN = "ukuran";
    private static final String TAG_GAMBAR = "gambar";
    private static final String TAG_WAKTU = "masa_berlaku";
    private static final String TAG_LAPORAN = "laporan";
    TextView waktuC, status;
    LinearLayout Lstatus;

    int[] dists;
    double[] latitude_r, longitude_r;
    String[] r_id, r_latitude, r_longitude, r_nomor, r_teks, r_alamat,
            r_detail, r_sipr, r_perusahaan, r_jenis, r_waktu, r_ukuran,
            r_gambar, r_laporan,
            r_id2, r_nomor2, r_teks2, r_alamat2, r_detail2,
            r_sipr2, r_perusahaan2, r_jenis2, r_ukuran2, r_gambar2;

    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        Bundle b=this.getIntent().getExtras();
        r_id =b.getStringArray("id");
        r_latitude =b.getStringArray("latitude");
        r_longitude =b.getStringArray("longitude");
        r_nomor = b.getStringArray("nomor");
        r_teks = b.getStringArray("teks");
        r_alamat = b.getStringArray("alamat");
        r_detail = b.getStringArray("detail");
        r_sipr = b.getStringArray("sipr");
        r_perusahaan = b.getStringArray("perusahaan");
        r_jenis = b.getStringArray("jenis");
        r_ukuran = b.getStringArray("ukuran");
        r_gambar = b.getStringArray("gambar");
        r_waktu = b.getStringArray("waktu");
        r_laporan = b.getStringArray("laporan");
        r_id2 =b.getStringArray("id2");
        r_nomor2 = b.getStringArray("nomor2");
        r_teks2 = b.getStringArray("teks2");
        r_alamat2 = b.getStringArray("alamat2");
        r_detail2 = b.getStringArray("detail2");
        r_sipr2 = b.getStringArray("sipr2");
        r_perusahaan2 = b.getStringArray("perusahaan2");
        r_jenis2 = b.getStringArray("jenis2");
        r_ukuran2 = b.getStringArray("ukuran2");
        r_gambar2 = b.getStringArray("gambar2");

        read();
        b_SV = (Button) findViewById(R.id.f_SV);
        b_SE = (Button) findViewById(R.id.f_SE);
        b_BV = (Button) findViewById(R.id.f_BV);
        b_BE = (Button) findViewById(R.id.f_BE);
        I_BE = (View) findViewById(R.id.f_BE);
        b_baru = (Button) findViewById(R.id.button_current);
        b_daftar = (Button) findViewById(R.id.button_list);
        b_cari = (Button) findViewById(R.id.button_ar);
        b_tambah = (Button) findViewById(R.id.button_tambah);
        b_ganti = (Button) findViewById(R.id.button_ganti);
        waktuC = (TextView)findViewById(R.id.tekswaktu);
        status = (TextView) findViewById(R.id.text_status);
        Lstatus = (LinearLayout) findViewById(R.id.l_status);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        getCurrentDateTime = sdf.format(c.getTime());
    }



    public void read(){

    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        starttime = System.currentTimeMillis();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

        GPSTracker gps = new GPSTracker(this);

        getData();

        if(gps.canGetLocation()) {
            lat = gps.getLatitude();
            lng = gps.getLongitude();
            latLng = new LatLng(lat, lng);

            CameraPosition newCamPos = new CameraPosition(new LatLng(lat,lng),
                    15.5f,
                    mMap.getCameraPosition().tilt, //use old tilt
                    mMap.getCameraPosition().bearing); //use old bearing
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCamPos), 3000, null);

        }

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng a) {
                latt = a.latitude;
                lngg = a.longitude;
                b_cari.setVisibility(View.INVISIBLE);
                b_baru.setVisibility(View.INVISIBLE);
                b_daftar.setVisibility(View.INVISIBLE);
                b_tambah.setVisibility(View.VISIBLE);
                b_ganti.setVisibility(View.VISIBLE);
                if (m == null){
                    Toast.makeText(MapsActivity.this, String.valueOf(a), Toast.LENGTH_LONG).show();
                    m = mMap.addMarker(new MarkerOptions()
                                    .position(a)
                                    .title("Laporkan")
//                                .icon(BitmapDescriptorFactory.defaultMarker(biru))
                                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker))
                    );

                    CameraPosition newCamPos = new CameraPosition(new LatLng(latt,lngg),
                            18.5f,
                            mMap.getCameraPosition().tilt, //use old tilt
                            mMap.getCameraPosition().bearing); //use old bearing
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCamPos), 1000, null);
                    mar = true;
                }
                else {
                    m.remove();
                    Toast.makeText(MapsActivity.this, String.valueOf(a), Toast.LENGTH_LONG).show();
                    m = mMap.addMarker(new MarkerOptions()
                                    .position(a)
                                    .title("Laporkan")
//                                .icon(BitmapDescriptorFactory.defaultMarker(biru))
                                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker))
                    );
                    CameraPosition newCamPos = new CameraPosition(new LatLng(latt,lngg),
                            18.5f,
                            mMap.getCameraPosition().tilt, //use old tilt
                            mMap.getCameraPosition().bearing); //use old bearing
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCamPos), 500, null);
                    mar = true;
                }
            }
        });

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                for (int g = 0; g < markerfix.length; g++){
                    if (marker.equals(markerfix[g])){
                        Intent i = new Intent(MapsActivity.this, InfoReklame.class);

                        i.putExtra("id_R", r_id[g]);
                        i.putExtra("latitude", r_latitude[g]);
                        i.putExtra("longitude", r_longitude[g]);
                        i.putExtra("no_F", r_nomor[g]);
                        i.putExtra("teks_RE", r_teks[g]);
                        i.putExtra("alamat_R", r_alamat[g]);
                        i.putExtra("detail_R", r_detail[g]);
                        i.putExtra("sipr_R", r_sipr[g]);
                        i.putExtra("nama_perusahaan", r_perusahaan[g]);
                        i.putExtra("jenis_R", r_jenis[g]);
                        i.putExtra("ukuran", r_ukuran[g]);
                        i.putExtra("gambar", r_gambar[g]);
                        i.putExtra("masa_R", r_waktu[g]);
                        i.putExtra("laporan", r_laporan[g]);

                        startActivity(i);
                    }
                }
                if (marker.equals(m)){
                    Intent w = new Intent(MapsActivity.this, LaporanActivity.class);
                    startActivity(w);
                }
            }
        });

        remarker();
        markerBV();
        Lstatus.setVisibility(View.VISIBLE);
        status.setText("Belum Diperiksa");
        Lstatus.setBackgroundColor(Color.rgb(255, 102, 0));
        flag = 1;
    }

    public void remarker(){
        if (flag == 1) {
            for (int i = 0; i < r_latitude.length; i++) {
                markerfix[i].remove();
            }
        }
    }

    public void markerBE(){
        String Teks;
        String wawaktu, peperu;
        markerfix = new Marker[r_latitude.length];
        latitude_r = new double[r_latitude.length];
        longitude_r = new double[r_longitude.length];


        for (int i = 0; i < r_latitude.length; i++) {
            double lalat = Double.parseDouble(r_latitude[i]);
            double lolong = Double.parseDouble(r_longitude[i]);
            wawaktu = r_waktu[i];
            Teks = r_teks[i];
            peperu = r_perusahaan[i];

            latitude_r[i] = lalat;
            longitude_r[i] = lolong;

            LatLng REK = new LatLng(lalat, lolong);
            LatLng zero = new LatLng(0, 0);

            if (getCurrentDateTime.compareTo(wawaktu) < 0) {
                markerA1 = mMap.addMarker(new MarkerOptions()
                                .position(REK)
                                .title(Teks)
                                .snippet("BELUM")
                                .icon(BitmapDescriptorFactory.defaultMarker(biru))
//                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker))
                );
                markerfix[i] = markerA1;
            }
            else {
                markerA1 = mMap.addMarker(new MarkerOptions()
                                .position(zero)
                                .title(Teks)
                                .snippet("BELUM")
                                .icon(BitmapDescriptorFactory.defaultMarker(biru))
//                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker))
                );
                markerA1.setVisible(false);
                markerfix[i] = markerA1;
            }
        }
    }

    public void markerSE(){
        String Teks;
        String wawaktu, peperu;
        markerfix = new Marker[r_latitude.length];
        latitude_r = new double[r_latitude.length];
        longitude_r = new double[r_longitude.length];


        for (int i = 0; i < r_latitude.length; i++) {
            double lalat = Double.parseDouble(r_latitude[i]);
            double lolong = Double.parseDouble(r_longitude[i]);
            wawaktu = r_waktu[i];
            Teks = r_teks[i];
            peperu = r_perusahaan[i];

            latitude_r[i] = lalat;
            longitude_r[i] = lolong;

            LatLng REK = new LatLng(lalat, lolong);
            LatLng zero = new LatLng(0, 0);

            if (getCurrentDateTime.compareTo(wawaktu) < 0) {
                markerA1 = mMap.addMarker(new MarkerOptions()
                                .position(zero)
                                .title(Teks)
                                .snippet("BELUM")
                                .icon(BitmapDescriptorFactory.defaultMarker(biru))
//                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker))
                );
                markerA1.setVisible(false);
                markerfix[i] = markerA1;
            }
            else {
                markerA1 = mMap.addMarker(new MarkerOptions()
                                .position(REK)
                                .title(Teks)
                                .snippet("BELUM")
                                .icon(BitmapDescriptorFactory.defaultMarker(merah))
//                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker))
                );
                markerfix[i] = markerA1;
            }
        }
    }

    public void markerBV(){
        String Teks;
        String wawaktu, peperu;
        markerfix = new Marker[r_latitude.length];
        latitude_r = new double[r_latitude.length];
        longitude_r = new double[r_longitude.length];


        for (int i = 0; i < r_latitude.length; i++) {
            double lalat = Double.parseDouble(r_latitude[i]);
            double lolong = Double.parseDouble(r_longitude[i]);
            wawaktu = r_waktu[i];
            Teks = r_teks[i];
            peperu = r_perusahaan[i];

            latitude_r[i] = lalat;
            longitude_r[i] = lolong;

            LatLng REK = new LatLng(lalat, lolong);
            LatLng zero = new LatLng(0, 0);

            if (r_laporan[i].equals("0")) {
                markerA1 = mMap.addMarker(new MarkerOptions()
                                .position(REK)
                                .title(Teks)
                                .snippet("BELUM")
                                .icon(BitmapDescriptorFactory.defaultMarker(orange))
//                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker))
                );
                markerfix[i] = markerA1;
            }
            else {
                markerA1 = mMap.addMarker(new MarkerOptions()
                                .position(zero)
                                .title(Teks)
                                .snippet("BELUM")
                                .icon(BitmapDescriptorFactory.defaultMarker(merah))
//                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker))
                );
                markerA1.setVisible(false);
                markerfix[i] = markerA1;
            }
        }
        EndTime = System.currentTimeMillis();
        double hasil = EndTime - starttime;
        waktuC.setText(String.valueOf(hasil/1000));
        waktuC.setVisibility(View.VISIBLE);
    }

    public void markerSV(){
        String Teks;
        markerfix = new Marker[r_latitude.length];
        latitude_r = new double[r_latitude.length];
        longitude_r = new double[r_longitude.length];

        for (int i = 0; i < r_latitude.length; i++) {
            double lalat = Double.parseDouble(r_latitude[i]);
            double lolong = Double.parseDouble(r_longitude[i]);
            Teks = r_teks[i];

            latitude_r[i] = lalat;
            longitude_r[i] = lolong;

            LatLng REK = new LatLng(lalat, lolong);
            LatLng zero = new LatLng(0, 0);

            if (r_laporan[i].equals("0")) {
                markerA1 = mMap.addMarker(new MarkerOptions()
                                .position(zero)
                                .title(Teks)
                                .snippet("BELUM")
                                .icon(BitmapDescriptorFactory.defaultMarker(merah))
//                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker))
                );
                markerA1.setVisible(false);
                markerfix[i] = markerA1;
            }
            else {
                markerA1 = mMap.addMarker(new MarkerOptions()
                                .position(REK)
                                .title(Teks)
                                .snippet("SUDAH")
                                .icon(BitmapDescriptorFactory.defaultMarker(hijau))
//                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker))
                );
                markerfix[i] = markerA1;
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_filter, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.f_BE:
                if (item.isChecked()) item.setChecked(false);
                else item.setChecked(true);
                remarker();
                markerBE();
                Lstatus.setVisibility(View.VISIBLE);
                status.setText("Belum Expired");
                Lstatus.setBackgroundColor(Color.BLUE);
                return true;
            case R.id.f_BV:
                if (item.isChecked()) item.setChecked(false);
                else item.setChecked(true);
                remarker();
                markerBV();
                Lstatus.setVisibility(View.VISIBLE);
                status.setText("Belum Diperiksa");
                Lstatus.setBackgroundColor(Color.rgb(255, 102, 0));
                return true;
            case R.id.f_SE:
                if (item.isChecked()) item.setChecked(false);
                else item.setChecked(true);
                remarker();
                markerSE();
                Lstatus.setVisibility(View.VISIBLE);
                status.setText("Sudah Expired");
                Lstatus.setBackgroundColor(Color.RED);
                return true;
            case R.id.f_SV:
                if (item.isChecked()) item.setChecked(false);
                else item.setChecked(true);
                remarker();
                markerSV();
                Lstatus.setVisibility(View.VISIBLE);
                status.setText("Sudah Diperiksa");
                Lstatus.setBackgroundColor(Color.GREEN);
                return true;
            case android.R.id.home:
                //Write your logic here
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void listgambar(View view) {
        Intent intent = new Intent(this, ListReklameActivity.class);

        Bundle c = new Bundle();

        c.putIntArray("dists", dists);
        c.putStringArray("id", r_id);
        c.putStringArray("latitude", r_latitude);
        c.putStringArray("longitude", r_longitude);
        c.putStringArray("nomor", r_nomor);
        c.putStringArray("teks", r_teks);
        c.putStringArray("alamat", r_alamat);
        c.putStringArray("detail", r_detail);
        c.putStringArray("sipr", r_sipr);
        c.putStringArray("perusahaan", r_perusahaan);
        c.putStringArray("jenis", r_jenis);
        c.putStringArray("ukuran", r_ukuran);
        c.putStringArray("gambar", r_gambar);
        c.putStringArray("waktu", r_waktu);
        c.putStringArray("laporan", r_laporan);

        intent.putExtras(c);

        getData();

        startActivity(intent);
    }

    public void AR(View view){
        Intent i = new Intent(MapsActivity.this, InfoReklame.class);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        GPSTracker gps = new GPSTracker(this);
        if(gps.canGetLocation()) {
            lat = gps.getLatitude();
            lng = gps.getLongitude();
        }

        int min = dekat(lat, lng);

        i.putExtra("id_R", r_id[min]);
        i.putExtra("latitude", r_latitude[min]);
        i.putExtra("longitude", r_longitude[min]);
        i.putExtra("no_F", r_nomor[min]);
        i.putExtra("teks_RE", r_teks[min]);
        i.putExtra("alamat_R", r_alamat[min]);
        i.putExtra("detail_R", r_detail[min]);
        i.putExtra("sipr_R", r_sipr[min]);
        i.putExtra("nama_perusahaan", r_perusahaan[min]);
        i.putExtra("jenis_R", r_jenis[min]);
        i.putExtra("ukuran", r_ukuran[min]);
        i.putExtra("gambar", r_gambar[min]);
        i.putExtra("masa_R", r_waktu[min]);

        startActivity(i);
    }

    public void listtambahkoor(View view) {
        Intent intent = new Intent(this, UploadKoorActivity.class);
        Bundle c = new Bundle();

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        mMap.setMyLocationEnabled(true);
        GPSTracker gps = new GPSTracker(this);
        if(gps.canGetLocation()) {
            lat = gps.getLatitude();
            lng = gps.getLongitude();
        }
        latLng = new LatLng(lat, lng);

        c.putStringArray("id2", r_id2);
        c.putStringArray("nomor2", r_nomor2);
        c.putStringArray("teks2", r_teks2);
        c.putStringArray("alamat2", r_alamat2);
        c.putStringArray("detail2", r_detail2);
        c.putStringArray("sipr2", r_sipr2);
        c.putStringArray("perusahaan2", r_perusahaan2);
        c.putStringArray("jenis2", r_jenis2);
        c.putStringArray("ukuran2", r_ukuran2);
        c.putStringArray("gambar2", r_gambar2);
        c.putDouble("latitude", lat);
        c.putDouble("longitude", lng);
        intent.putExtras(c);

        startActivity(intent);
    }

    public void tombol_tambah(View view){
        Intent intent = new Intent(this, UploadKoorActivity.class);
        int flag = 1;
        Bundle c = new Bundle();

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        mMap.setMyLocationEnabled(true);

        latLng = new LatLng(latt, lngg);

        c.putStringArray("id2", r_id2);
        c.putStringArray("nomor2", r_nomor2);
        c.putStringArray("teks2", r_teks2);
        c.putStringArray("alamat2", r_alamat2);
        c.putStringArray("detail2", r_detail2);
        c.putStringArray("sipr2", r_sipr2);
        c.putStringArray("perusahaan2", r_perusahaan2);
        c.putStringArray("jenis2", r_jenis2);
        c.putStringArray("ukuran2", r_ukuran2);
        c.putStringArray("gambar2", r_gambar2);
        c.putDouble("latitude", latt);
        c.putDouble("longitude", lngg);
        intent.putExtras(c);

        startActivity(intent);
    }

    public void tombol_ganti(View view){
        Intent intent = new Intent(this, ListReklameActivity.class);
        int flag = 1;

        Bundle c = new Bundle();

        c.putIntArray("dists", dists);
        c.putStringArray("id", r_id);
        c.putStringArray("latitude", r_latitude);
        c.putStringArray("longitude", r_longitude);
        c.putStringArray("nomor", r_nomor);
        c.putStringArray("teks", r_teks);
        c.putStringArray("alamat", r_alamat);
        c.putStringArray("detail", r_detail);
        c.putStringArray("sipr", r_sipr);
        c.putStringArray("perusahaan", r_perusahaan);
        c.putStringArray("jenis", r_jenis);
        c.putStringArray("ukuran", r_ukuran);
        c.putStringArray("gambar", r_gambar);
        c.putStringArray("waktu", r_waktu);
        c.putStringArray("laporan", r_laporan);
        c.putDouble("latt", latt);
        c.putDouble("lngg", lngg);
        c.putInt("flag", flag);

        intent.putExtras(c);

        getData();

        startActivity(intent);
    }

    public int dekat(double lats,double longs){
        dists = new int[latitude_r.length];
        double latatata, longitata;

        for (int i = 0; i < latitude_r.length; i++) {
            latatata = latitude_r[i];
            longitata = longitude_r[i];
            dists[i] = (int) distFrom(lats, longs, latatata, longitata);
        }
        min = findMinIdx(dists);
        return min;
    }

    public int findMinIdx(int[] numbers) {
        if (numbers == null || numbers.length == 0) return -1; // Saves time for empty array
        // As pointed out by ZouZou, you can save an iteration by assuming the first index is the smallest
        int minVal = numbers[0]; // Keeps a running count of the smallest value so far
        int minIdx = 0; // Will store the index of minVal
        for(int idx=1; idx<numbers.length; idx++) {
            if(numbers[idx] < minVal) {
                minVal = numbers[idx];
                minIdx = idx;
            }
        }
        return minIdx;
    }

    public static double distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = (double) (earthRadius * c);


        return dist;
    }

    @Override
    public void onLocationChanged(Location location) {

        latLng = new LatLng(location.getLatitude(), location.getLongitude());

        lat = location.getLatitude();
        lng = location.getLongitude();

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

        Toast.makeText(MapsActivity.this, String.valueOf(latLng), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        if (mar == false){
            new AlertDialog.Builder(this)
                    .setIcon(R.mipmap.ic_launcher)
                    .setTitle("EXIT")
                    .setMessage("Are you sure you want to close this activity?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }

                    })
                    .setNegativeButton("No", null)
                    .show();
        }
        else {
            m.remove();
            mar = false;
            b_cari.setVisibility(View.VISIBLE);
            b_baru.setVisibility(View.VISIBLE);
            b_daftar.setVisibility(View.VISIBLE);
            b_tambah.setVisibility(View.INVISIBLE);
            b_ganti.setVisibility(View.INVISIBLE);
        }

    }

    public void getData(){
        class GetDataJSON extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());
                HttpPost httppost = new HttpPost(Config.GET_REKLAME);

                // Depends on your web service
                httppost.setHeader("Content-type", "application/json");

                InputStream inputStream = null;
                String result = null;
                try {
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();

                    inputStream = entity.getContent();
                    // json is UTF-8 by default
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();

                    String line = null;
                    while ((line = reader.readLine()) != null)
                    {
                        sb.append(line + "\n");
                    }
                    result = sb.toString();
                } catch (Exception e) {
                    // Oops
                }
                finally {
                    try{if(inputStream != null)inputStream.close();}catch(Exception squish){}
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result){
                myJSON=result;
                convert();
            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute();
    }

    protected void convert(){
        rek_laporan = new ArrayList<String>();
        rek_id = new ArrayList<String>();
        rek_latitude = new ArrayList<String>();
        rek_longitude = new ArrayList<String>();
        rek_nomor = new ArrayList<String>();
        rek_teks = new ArrayList<String>();
        rek_alamat = new ArrayList<String>();
        rek_detail = new ArrayList<String>();
        rek_sipr = new ArrayList<String>();
        rek_perusahaan = new ArrayList<String>();
        rek_jenis = new ArrayList<String>();
        rek_ukuran = new ArrayList<String>();
        rek_gambar = new ArrayList<String>();
        rek_waktu = new ArrayList<String>();
        rek_id2 = new ArrayList<String>();
        rek_nomor2 = new ArrayList<String>();
        rek_teks2 = new ArrayList<String>();
        rek_alamat2 = new ArrayList<String>();
        rek_detail2 = new ArrayList<String>();
        rek_sipr2 = new ArrayList<String>();
        rek_perusahaan2 = new ArrayList<String>();
        rek_jenis2 = new ArrayList<String>();
        rek_ukuran2 = new ArrayList<String>();
        rek_gambar2 = new ArrayList<String>();


        try {
            JSONObject jsonObj = new JSONObject(myJSON);
            peoples = jsonObj.getJSONArray(TAG_RESULTS);

            for (int i = 0; i < peoples.length(); i++) {
                JSONObject c = peoples.getJSONObject(i);
                String id = c.getString(TAG_ID);
                String lat = c.getString(TAG_LAT);
                String lng = c.getString(TAG_LONG);
                String noformulir = c.getString(TAG_NO);
                String teks_reklame = c.getString(TAG_TEKS);
                String alamat_rek = c.getString(TAG_ALAMAT);
                String detail_lokasi = c.getString(TAG_DETAIL);
                String no_sipr = c.getString(TAG_SIPR);
                String nama_perusahaan = c.getString(TAG_PERUSAHAAN);
                String jenis_rek = c.getString(TAG_JENIS);
                String ukuran_rek = c.getString(TAG_UKURAN);
                String gambar_rek = c.getString(TAG_GAMBAR);
                String reklameDate = c.getString(TAG_WAKTU);
                String laporan_rek = c.getString(TAG_LAPORAN);

                if (lat.equals("0")){
                    rek_id2.add(id);
                    rek_nomor2.add(noformulir);
                    rek_teks2.add(teks_reklame);
                    rek_alamat2.add(alamat_rek);
                    rek_detail2.add(detail_lokasi);
                    rek_sipr2.add(no_sipr);
                    rek_perusahaan2.add(nama_perusahaan);
                    rek_jenis2.add(jenis_rek);
                    rek_ukuran2.add(ukuran_rek);
                    rek_gambar2.add(gambar_rek);
                }
                else {
                    rek_id.add(id);
                    rek_latitude.add(lat);
                    rek_longitude.add(lng);
                    rek_nomor.add(noformulir);
                    rek_teks.add(teks_reklame);
                    rek_alamat.add(alamat_rek);
                    rek_detail.add(detail_lokasi);
                    rek_sipr.add(no_sipr);
                    rek_perusahaan.add(nama_perusahaan);
                    rek_jenis.add(jenis_rek);
                    rek_ukuran.add(ukuran_rek);
                    rek_gambar.add(gambar_rek);
                    rek_waktu.add(reklameDate);
                    rek_laporan.add(laporan_rek);
                }


            }
            r_id = rek_id.toArray(new String[rek_id.size()]);
            r_latitude = rek_latitude.toArray(new String[rek_latitude.size()]);
            r_longitude = rek_longitude.toArray(new String[rek_latitude.size()]);
            r_nomor = rek_nomor.toArray(new String[rek_nomor.size()]);
            r_teks = rek_teks.toArray(new String[rek_teks.size()]);
            r_alamat = rek_alamat.toArray(new  String[rek_alamat.size()]);
            r_detail = rek_detail.toArray(new String[rek_detail.size()]);
            r_sipr = rek_sipr.toArray(new String[rek_sipr.size()]);
            r_perusahaan = rek_perusahaan.toArray(new String[rek_perusahaan.size()]);
            r_jenis = rek_jenis.toArray(new String[rek_jenis.size()]);
            r_ukuran = rek_ukuran.toArray(new String[rek_ukuran.size()]);
            r_gambar = rek_gambar.toArray(new String[rek_gambar.size()]);
            r_waktu = rek_waktu.toArray(new String[rek_waktu.size()]);
            r_laporan = rek_laporan.toArray(new String[rek_laporan.size()]);

            r_id2 = rek_id2.toArray(new String[rek_id2.size()]);
            r_nomor2 = rek_nomor2.toArray(new String[rek_nomor2.size()]);
            r_teks2 = rek_teks2.toArray(new String[rek_teks2.size()]);
            r_alamat2 = rek_alamat2.toArray(new  String[rek_alamat2.size()]);
            r_detail2 = rek_detail2.toArray(new String[rek_detail2.size()]);
            r_sipr2 = rek_sipr2.toArray(new String[rek_sipr2.size()]);
            r_perusahaan2 = rek_perusahaan2.toArray(new String[rek_perusahaan2.size()]);
            r_jenis2 = rek_jenis2.toArray(new String[rek_jenis2.size()]);
            r_ukuran2 = rek_ukuran2.toArray(new String[rek_ukuran2.size()]);
            r_gambar2 = rek_gambar2.toArray(new String[rek_gambar2.size()]);




        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        getData();

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(500);
        mLocationRequest.setFastestInterval(500);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }
}
