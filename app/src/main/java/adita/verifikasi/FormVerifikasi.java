package adita.verifikasi;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import adita.verifikasi.Config.Config;
import adita.verifikasi.Entity.AndroidMultiPartEntity;

public class FormVerifikasi extends AppCompatActivity {
    String ID, NoF, TeksR, AlamatR, DetailR, Sipr, NamaP, JenisR, Ukuran, MasaBer, Komentar;
    String verif = "1";
    int check_teks = 0, check_lokasi = 0, check_jenis = 0, check_ukuran = 0, flag = 0;
    CheckBox verifTeks, verifLokasi, verifJenis, verifUkuran;
    private static final String TAG = FormVerifikasi.class.getSimpleName();
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private ProgressBar progressBar3;
    private Uri fileUri;
    private String filePath = null;
    EditText boxkomen;
    InputStream is=null;
    String result=null;
    String line=null;
    String teks_cek;
    String lokasi_cek;
    String jenis_cek;
    String ukuran_cek;
    ImageView fotoRek, imgRek;
    long totalSize = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_verifikasi);

        this.fotoRek = (ImageView)this.findViewById(R.id.imageForm);
        this.imgRek = (ImageView)this.findViewById(R.id.imgPreview);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        Bundle b=this.getIntent().getExtras();
        ID =b.getString("id");
        NoF = b.getString("nomor");
        TeksR = b.getString("teks");
        AlamatR = b.getString("alamat");
        DetailR = b.getString("detail");
        Sipr = b.getString("sipr");
        NamaP = b.getString("perusahaan");
        JenisR = b.getString("jenis");
        Ukuran = b.getString("ukuran");
        MasaBer = b.getString("waktu");

        TextView id_teks = (TextView)findViewById(R.id.textVeriTeks);
        TextView id_lokasi = (TextView)findViewById(R.id.textVeriAlamat);
        TextView id_jenis = (TextView)findViewById(R.id.textVeriJenis);
        TextView id_ukuran = (TextView)findViewById(R.id.textVeriUkuran);

        progressBar3 = (ProgressBar) findViewById(R.id.progressBar3);


        verifTeks = (CheckBox)findViewById(R.id.checkteks);
        verifLokasi = (CheckBox)findViewById(R.id.checkLokasi);
        verifJenis = (CheckBox)findViewById(R.id.checkJenis);
        verifUkuran = (CheckBox)findViewById(R.id.checkUkuran);

        boxkomen = (EditText) findViewById(R.id.komen);

        id_teks.setText(TeksR);
        id_lokasi.setText(AlamatR);
        id_jenis.setText(JenisR);
        id_ukuran.setText(Ukuran);
    }

//    public void gambarVerif(View view){
//        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//        startActivityForResult(cameraIntent, CAMERA_REQUEST);
//    }

//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
//            photo = (Bitmap) data.getExtras().get("data");
//            fotoRek.setImageBitmap(photo);
//        }
//    }

    public void upload(View view){
        if (flag == 1) {
            new AlertDialog.Builder(this)
                    .setTitle("UPLOAD")
                    .setMessage("Apakah anda yakin untuk melakukan verifikasi?")
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            new UploadFileToServer().execute();
                            new Thread() {
                                public void run() {
                                    Komentar = boxkomen.getText().toString();
                                    if (verifTeks.isChecked()) {
                                        check_teks = 1;
                                    } else {
                                        check_teks = 0;
                                    }
                                    if (verifLokasi.isChecked()) {
                                        check_lokasi = 1;
                                    } else {
                                        check_lokasi = 0;
                                    }
                                    if (verifJenis.isChecked()) {
                                        check_jenis = 1;
                                    } else {
                                        check_jenis = 0;
                                    }
                                    if (verifUkuran.isChecked()) {
                                        check_ukuran = 1;
                                    } else {
                                        check_ukuran = 0;
                                    }

                                    teks_cek = String.valueOf(check_teks);
                                    lokasi_cek = String.valueOf(check_lokasi);
                                    jenis_cek = String.valueOf(check_jenis);
                                    ukuran_cek = String.valueOf(check_ukuran);
//                                Toast.makeText(FormVerifikasi.this, Komentar +  "\n" + ID + "\n" + check_lokasi + "\n" + check_jenis + "\n" + check_ukuran, Toast.LENGTH_LONG).show();

                                    new Thread() {
                                        public void run() {
                                            insert();
                                        }
                                    }.start();
                                }
                            }.start();
                        }


                    })
                    .setNegativeButton("Tidak", null)
                    .show();
        }
        else{
            Toast.makeText(FormVerifikasi.this, "Tambahkan foto terlebih dahulu", Toast.LENGTH_LONG).show();
        }
    }

    public void insert()
    {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        nameValuePairs.add(new BasicNameValuePair("id_veri",ID));
        nameValuePairs.add(new BasicNameValuePair("teks_verif",teks_cek));
        nameValuePairs.add(new BasicNameValuePair("lokasi_verif",lokasi_cek));
        nameValuePairs.add(new BasicNameValuePair("jenis_verif",jenis_cek));
        nameValuePairs.add(new BasicNameValuePair("ukuran_verif",ukuran_cek));
        nameValuePairs.add(new BasicNameValuePair("komentar",Komentar));
        nameValuePairs.add(new BasicNameValuePair("verif",verif));

        try
        {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Config.PUT_VERIF);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 1", e.toString());

        }

        try
        {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success : " + result);
        }
        catch(Exception e)
        {
            Log.e("Fail 2", e.toString());
        }

        Log.e("Result", result);
        try
        {

        }
        catch(Exception e)
        {
            Log.e("Fail 3", e.toString());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void gambarVerif(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Launching camera app to record video
     */

    /**
     * Here we store the file url as it will be null after returning from camera
     * app
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }



    /**
     * Receiving activity result method will be called after closing the camera
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                // successfully captured the image
                // launching upload activity
                launchUploadActivity(true);


            } else if (resultCode == RESULT_CANCELED) {

                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();

            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }

        }
    }

    private void launchUploadActivity(boolean isImage){
        if (isImage) {
//            imgPreview.setVisibility(View.VISIBLE);
            // bimatp factory
            BitmapFactory.Options options = new BitmapFactory.Options();

            // down sizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 8;

            final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(), options);

            fotoRek.setVisibility(View.INVISIBLE);
            imgRek.setVisibility(View.VISIBLE);
            imgRek.setImageBitmap(bitmap);
            flag = 1;
        }
    }

    /**
     * ------------ Helper Methods ----------------------
     * */

    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                Config.IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + Config.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            // setting progress bar to zero

            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Making progress bar visible

            progressBar3.setVisibility(View.VISIBLE);

            // updating progress bar value


            // updating percentage value

        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Config.VERIF_UPLOAD_URL);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 500));
                            }
                        });

                File sourceFile = new File(fileUri.getPath());

                // Adding file data to http body
                entity.addPart("image", new FileBody(sourceFile));

                // Extra parameters if you want to pass to server
                entity.addPart("website",
                        new StringBody("www.ve.info"));
                entity.addPart("email", new StringBody(ID));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e(TAG, "Response from server: " + result);

            // showing the server response in an alert dialog
            showAlert(result);
//            txtSukses.setVisibility(View.VISIBLE);
//            txtSukses.setText("SUKSES");
            progressBar3.setVisibility(View.INVISIBLE);

            super.onPostExecute(result);
        }

    }

    /**
     * Method to show alert dialog
     * */
    private void showAlert(String message) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage(message).setTitle("Response from Servers")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // do nothing
                    }
                });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

}
