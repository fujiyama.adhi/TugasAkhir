package adita.verifikasi.Config;

/**
 * Created by adi on 22/04/2017.
 */

public class Config {
    public static final String FILE_UPLOAD_URL = "http://computer-its.com/TA/Adi/AndroidFileUpload/fileUpload.php";

    public static final String VERIF_UPLOAD_URL = "http://computer-its.com/TA/Adi/AndroidFileUpload/verifUpload.php";

    public static final String LAPOR_UPLOAD_URL = "http://computer-its.com/TA/Adi/AndroidFileUpload/laporUpload.php";

    public static final String PUT_VERIF = "http://computer-its.com/TA/Adi/reklame/put_verifikasi.php";

    public static final String GET_REKLAME = "http://computer-its.com/TA/Adi/reklame/get_reklamebaru.php";

    public static final String LOGIN_USER = "http://computer-its.com/TA/Adi/reklame/login.inc.php";

    public static final String UPDATE_KOORDINAT = "http://computer-its.com/TA/Adi/reklame/update_koordinat.php";

    public static final String SIGNUP = "http://computer-its.com/TA/Adi/reklame/put_registrasi.php";

    // Directory name to store captured images and videos
    public static final String IMAGE_DIRECTORY_NAME = "Android File Upload";
}
