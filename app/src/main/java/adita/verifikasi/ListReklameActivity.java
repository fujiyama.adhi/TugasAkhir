package adita.verifikasi;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import adita.verifikasi.Adapter.CustomArrayAdapter;
import adita.verifikasi.Object.Reklame;

public class ListReklameActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView mListView;
    List<Reklame> reklame_list= new ArrayList<Reklame>();
    int flag = 0;
    ImageView img;
    double lat, lng, latt, lngg;
    Reklame[] reklames;

    private List<HashMap<String, String>> list_data = new ArrayList<>();


    String[] r_id, r_latitude, r_longitude, r_nomor, r_teks, r_alamat,
            r_detail, r_sipr, r_perusahaan, r_jenis, r_ukuran, r_gambar,
            r_waktu, r_laporan, r_dists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_reklame);

        Bundle b = this.getIntent().getExtras();
        r_dists = b.getStringArray("dists");
        r_id = b.getStringArray("id");
        r_latitude = b.getStringArray("latitude");
        r_longitude = b.getStringArray("longitude");
        r_nomor = b.getStringArray("nomor");
        r_teks = b.getStringArray("teks");
        r_alamat = b.getStringArray("alamat");
        r_detail = b.getStringArray("detail");
        r_sipr = b.getStringArray("sipr");
        r_perusahaan = b.getStringArray("perusahaan");
        r_jenis = b.getStringArray("jenis");
        r_ukuran = b.getStringArray("ukuran");
        r_gambar = b.getStringArray("gambar");
        r_waktu = b.getStringArray("waktu");
        r_laporan = b.getStringArray("laporan");
        latt = b.getDouble("latt");
        lngg = b.getDouble("lngg");

        flag = b.getInt("flag");

        conversi();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        img = (ImageView) findViewById(R.id.gambar_reklame_list);

        mListView = (ListView) findViewById(R.id.list_view);
        mListView.setOnItemClickListener(this);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 1 second
                        mListView.setAdapter(new CustomArrayAdapter(ListReklameActivity.this,reklame_list));
                    }
                }, 500);
            }
        });

    }

    public void conversi() {
        GPSTracker gps = new GPSTracker(this);
        if (gps.canGetLocation()) {
            lat = gps.getLatitude();
            lng = gps.getLongitude();
        }
        reklames = new Reklame[r_id.length];

        for (int i = 0; i < r_id.length; i++) {
            double r_lat = Double.parseDouble(r_latitude[i]);
            double r_long = Double.parseDouble(r_longitude[i]);
            int dist = (int) distFrom(lat, lng, r_lat, r_long);
            double distt = dist;


            reklames[i] = new Reklame(r_id[i], r_lat, r_long, r_nomor[i], r_teks[i], r_alamat[i],
                        r_detail[i], r_sipr[i], r_perusahaan[i], r_jenis[i], r_ukuran[i],
                        r_gambar[i], r_waktu[i], r_laporan[i], distt);
        }

        Arrays.sort(reklames, Reklame.ReklameJarakComparator);
        for (int i = 0; i<reklames.length; i++){
            reklame_list.add(reklames[i]);
        }
    }

    public static double distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = (double) (earthRadius * c);

        return dist;
    }


    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

//        Toast.makeText(this, mAndroidMapList.get(i).get(KEY_TEKS),Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, InfoReklame.class);

        Bundle bundle = new Bundle();
        bundle.putString("id_R", reklame_list.get(i).getReklame_id());
        if (flag == 0){
            bundle.putDouble("latitude", reklame_list.get(i).getReklame_latitude());
            bundle.putDouble("longitude", reklame_list.get(i).getReklame_longitude());
        }
        else {
            bundle.putDouble("latitude", latt);
            bundle.putDouble("longitude", lngg);
        }
        bundle.putString("no_F", reklame_list.get(i).getReklame_nomor());
        bundle.putString("teks_RE", reklame_list.get(i).getReklame_teks());
        bundle.putString("alamat_R", reklame_list.get(i).getReklame_alamat());
        bundle.putString("detail_R", reklame_list.get(i).getReklame_detail());
        bundle.putString("sipr_R", reklame_list.get(i).getReklame_sipr());
        bundle.putString("nama_perusahaan", reklame_list.get(i).getReklame_perusahaan());
        bundle.putString("jenis_R", reklame_list.get(i).getReklame_jenis());
        bundle.putString("ukuran", reklame_list.get(i).getReklame_ukuran());
        bundle.putString("gambar", reklame_list.get(i).getReklame_gambar());
        bundle.putString("masa_R", reklame_list.get(i).getReklame_waktu());
        bundle.putString("laporan", reklame_list.get(i).getReklame_laporan());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
