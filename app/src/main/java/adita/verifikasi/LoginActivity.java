package adita.verifikasi;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import adita.verifikasi.Config.Config;

public class LoginActivity extends AppCompatActivity {

    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private EditText etEmail;
    private EditText etPassword;

    JSONArray result1 = null;

    String myJSON;
    ArrayList<String> rek_id, rek_latitude, rek_longitude, rek_nomor, rek_teks,
            rek_alamat, rek_detail, rek_sipr, rek_perusahaan, rek_jenis,
            rek_ukuran, rek_gambar, rek_waktu, rek_laporan,
            rek_id2, rek_nomor2, rek_teks2, rek_alamat2, rek_detail2, rek_sipr2,
            rek_perusahaan2, rek_jenis2, rek_ukuran2, rek_gambar2;
    String[] r_id, r_latitude, r_longitude, r_nomor, r_teks, r_alamat,
            r_detail, r_sipr, r_perusahaan, r_jenis, r_ukuran, r_gambar,
            r_waktu, r_laporan,
            r_id2, r_nomor2, r_teks2, r_alamat2,
            r_detail2, r_sipr2, r_perusahaan2, r_jenis2, r_ukuran2, r_gambar2;

    private static final String TAG_RESULT1 = "result";
    private static final String TAG_ID = "id";
    private static final String TAG_LAT = "latitude";
    private static final String TAG_LONG = "longitude";
    private static final String TAG_NO = "no_formulr";
    private static final String TAG_TEKS = "teks_reklame";
    private static final String TAG_ALAMAT = "alamat_reklame";
    private static final String TAG_DETAIL = "detail_lokasi";
    private static final String TAG_SIPR = "no_sipr";
    private static final String TAG_PERUSAHAAN = "nama_perusahaan";
    private static final String TAG_JENIS = "jenis";
    private static final String TAG_UKURAN = "ukuran";
    private static final String TAG_GAMBAR = "gambar";
    private static final String TAG_WAKTU = "masa_berlaku";
    private static final String TAG_LAPORAN = "laporan";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Get Reference to variables
        etEmail = (EditText) findViewById(R.id.eemail);
        etPassword = (EditText) findViewById(R.id.ppassword);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        getData();
    }

    public void sign_up(View view){
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

    // Triggers when LOGIN Button clicked
    public void checkLogin(View arg0) {

        // Get text from email and passord field
        final String email = etEmail.getText().toString();
        final String password = etPassword.getText().toString();

        // Initialize  AsyncLogin() class with email and password
        new AsyncLogin().execute(email, password);
    }

    private class AsyncLogin extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(LoginActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... params) {
            try {

                // Enter URL address where your php file resides
                url = new URL(Config.LOGIN_USER);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("username", params[0])
                        .appendQueryParameter("password", params[1]);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return (result.toString());

                } else {

                    return ("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();

            if (result.equalsIgnoreCase("true")) {
                /* Here launching another activity when login successful. If you persist login state
                use sharedPreferences of Android. and logout button to clear sharedPreferences.
                 */

                Intent intent = new Intent(LoginActivity.this, MapsActivity.class);
                Bundle c = new Bundle();

                c.putStringArray("id", r_id);
                c.putStringArray("latitude", r_latitude);
                c.putStringArray("longitude", r_longitude);
                c.putStringArray("nomor", r_nomor);
                c.putStringArray("teks", r_teks);
                c.putStringArray("alamat", r_alamat);
                c.putStringArray("detail", r_detail);
                c.putStringArray("sipr", r_sipr);
                c.putStringArray("perusahaan", r_perusahaan);
                c.putStringArray("jenis", r_jenis);
                c.putStringArray("ukuran", r_ukuran);
                c.putStringArray("gambar", r_gambar);
                c.putStringArray("waktu", r_waktu);
                c.putStringArray("laporan", r_laporan);
                c.putStringArray("id2", r_id2);
                c.putStringArray("nomor2", r_nomor2);
                c.putStringArray("teks2", r_teks2);
                c.putStringArray("alamat2", r_alamat2);
                c.putStringArray("detail2", r_detail2);
                c.putStringArray("sipr2", r_sipr2);
                c.putStringArray("perusahaan2", r_perusahaan2);
                c.putStringArray("jenis2", r_jenis2);
                c.putStringArray("ukuran2", r_ukuran2);
                c.putStringArray("gambar2", r_gambar2);

                intent.putExtras(c);
                startActivity(intent);
                LoginActivity.this.finish();

            } else if (result.equalsIgnoreCase("false")) {

                // If username and password does not match display a error message
                Toast.makeText(LoginActivity.this, "Invalid email or password", Toast.LENGTH_LONG).show();


            } else if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("unsuccessful")) {

                Toast.makeText(LoginActivity.this, "OOPs! Something went wrong. Connection Problem.", Toast.LENGTH_LONG).show();

            }
        }

    }

    public void getData() {
        class GetDataJSON extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());
                HttpPost httppost = new HttpPost(Config.GET_REKLAME);

                // Depends on your web service
                httppost.setHeader("Content-type", "application/json");

                InputStream inputStream = null;
                String result = null;
                try {
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();

                    inputStream = entity.getContent();
                    // json is UTF-8 by default
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();

                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    result = sb.toString();
                } catch (Exception e) {
                    // Oops
                } finally {
                    try {
                        if (inputStream != null) inputStream.close();
                    } catch (Exception squish) {
                    }
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                myJSON = result;
                showList();
            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute();
    }

    protected void showList() {
        rek_id = new ArrayList<String>();
        rek_latitude = new ArrayList<String>();
        rek_longitude = new ArrayList<String>();
        rek_nomor = new ArrayList<String>();
        rek_teks = new ArrayList<String>();
        rek_alamat = new ArrayList<String>();
        rek_detail = new ArrayList<String>();
        rek_sipr = new ArrayList<String>();
        rek_perusahaan = new ArrayList<String>();
        rek_jenis = new ArrayList<String>();
        rek_ukuran = new ArrayList<String>();
        rek_gambar = new ArrayList<String>();
        rek_waktu = new ArrayList<String>();
        rek_laporan = new ArrayList<String>();
        rek_id2 = new ArrayList<String>();
        rek_nomor2 = new ArrayList<String>();
        rek_teks2 = new ArrayList<String>();
        rek_alamat2 = new ArrayList<String>();
        rek_detail2 = new ArrayList<String>();
        rek_sipr2 = new ArrayList<String>();
        rek_perusahaan2 = new ArrayList<String>();
        rek_jenis2 = new ArrayList<String>();
        rek_ukuran2 = new ArrayList<String>();
        rek_gambar2 = new ArrayList<String>();

        try {
            JSONObject jsonObj = new JSONObject(myJSON);
            result1 = jsonObj.getJSONArray(TAG_RESULT1);
//            result2 = jsonObj.getJSONArray(TAG_RESULT2);

            for (int i = 0; i < result1.length(); i++) {
                JSONObject c = result1.getJSONObject(i);
                String id = c.getString(TAG_ID);
                String lat = c.getString(TAG_LAT);
                String lng = c.getString(TAG_LONG);
                String noformulir = c.getString(TAG_NO);
                String teks_reklame = c.getString(TAG_TEKS);
                String alamat_rek = c.getString(TAG_ALAMAT);
                String detail_lokasi = c.getString(TAG_DETAIL);
                String no_sipr = c.getString(TAG_SIPR);
                String nama_perusahaan = c.getString(TAG_PERUSAHAAN);
                String jenis_rek = c.getString(TAG_JENIS);
                String ukuran_rek = c.getString(TAG_UKURAN);
                String gambar_rek = c.getString(TAG_GAMBAR);
                String reklameDate = c.getString(TAG_WAKTU);
                String verif_rek = c.getString(TAG_LAPORAN);

                if (lat.equals("0")) {
                    rek_id2.add(id);
                    rek_nomor2.add(noformulir);
                    rek_teks2.add(teks_reklame);
                    rek_alamat2.add(alamat_rek);
                    rek_detail2.add(detail_lokasi);
                    rek_sipr2.add(no_sipr);
                    rek_perusahaan2.add(nama_perusahaan);
                    rek_jenis2.add(jenis_rek);
                    rek_ukuran2.add(ukuran_rek);
                    rek_gambar2.add(gambar_rek);
                } else {
                    rek_id.add(id);
                    rek_latitude.add(lat);
                    rek_longitude.add(lng);
                    rek_nomor.add(noformulir);
                    rek_teks.add(teks_reklame);
                    rek_alamat.add(alamat_rek);
                    rek_detail.add(detail_lokasi);
                    rek_sipr.add(no_sipr);
                    rek_perusahaan.add(nama_perusahaan);
                    rek_jenis.add(jenis_rek);
                    rek_ukuran.add(ukuran_rek);
                    rek_gambar.add(gambar_rek);
                    rek_waktu.add(reklameDate);
                    rek_laporan.add(verif_rek);
                }

            }
            r_id = rek_id.toArray(new String[rek_id.size()]);
            r_latitude = rek_latitude.toArray(new String[rek_latitude.size()]);
            r_longitude = rek_longitude.toArray(new String[rek_latitude.size()]);
            r_nomor = rek_nomor.toArray(new String[rek_nomor.size()]);
            r_teks = rek_teks.toArray(new String[rek_teks.size()]);
            r_alamat = rek_alamat.toArray(new String[rek_alamat.size()]);
            r_detail = rek_detail.toArray(new String[rek_detail.size()]);
            r_sipr = rek_sipr.toArray(new String[rek_sipr.size()]);
            r_perusahaan = rek_perusahaan.toArray(new String[rek_perusahaan.size()]);
            r_jenis = rek_jenis.toArray(new String[rek_jenis.size()]);
            r_ukuran = rek_ukuran.toArray(new String[rek_ukuran.size()]);
            r_gambar = rek_gambar.toArray(new String[rek_gambar.size()]);
            r_waktu = rek_waktu.toArray(new String[rek_waktu.size()]);
            r_laporan = rek_laporan.toArray(new String[rek_laporan.size()]);

            r_id2 = rek_id2.toArray(new String[rek_id2.size()]);
            r_nomor2 = rek_nomor2.toArray(new String[rek_nomor2.size()]);
            r_teks2 = rek_teks2.toArray(new String[rek_teks2.size()]);
            r_alamat2 = rek_alamat2.toArray(new String[rek_alamat2.size()]);
            r_detail2 = rek_detail2.toArray(new String[rek_detail2.size()]);
            r_sipr2 = rek_sipr2.toArray(new String[rek_sipr2.size()]);
            r_perusahaan2 = rek_perusahaan2.toArray(new String[rek_perusahaan2.size()]);
            r_jenis2 = rek_jenis2.toArray(new String[rek_jenis2.size()]);
            r_ukuran2 = rek_ukuran2.toArray(new String[rek_ukuran2.size()]);
            r_gambar2 = rek_gambar2.toArray(new String[rek_gambar2.size()]);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(R.mipmap.ic_launcher)
                .setTitle("EXIT")
                .setMessage("Are you sure you want to close this activity?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }
}
