package adita.verifikasi;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import adita.verifikasi.Config.Config;
import adita.verifikasi.Entity.AndroidMultiPartEntity;


public class InfoReklame extends AppCompatActivity {
    String ID, NoF, TeksR, AlamatR, DetailR, Sipr,
            NamaP, JenisR, ukuran, gambar, MasaBer, laporan,
            status, lat_baru, longi_baru;
    double Lati, Longi;
    InputStream is=null;
    String result=null;
    String line=null;
    ImageView img;
    Button b_updatefoto;
    private Uri fileUri;
    private ProgressBar progressBar;
    private ProgressBar progressBar2;
    private TextView txtPercentage;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String TAG = InfoReklame.class.getSimpleName();
    long totalSize = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_reklame);



        Intent intent = getIntent();
        if(null!=intent.getExtras()){
            ID = intent.getExtras().getString("id_R");
//            Lati = intent.getExtras().getString("latitude");
//            Longi = intent.getExtras().getString("longitude");
            Lati = intent.getExtras().getDouble("latitude");
            Longi = intent.getExtras().getDouble("longitude");
            NoF = intent.getExtras().getString("no_F");
            TeksR = intent.getExtras().getString("teks_RE");
            AlamatR = intent.getExtras().getString("alamat_R");
            DetailR = intent.getExtras().getString("detail_R");
            Sipr = intent.getExtras().getString("sipr_R");
            NamaP = intent.getExtras().getString("nama_perusahaan");
            JenisR = intent.getExtras().getString("jenis_R");
            ukuran = intent.getExtras().getString("ukuran");
            gambar = intent.getExtras().getString("gambar");
            MasaBer = intent.getExtras().getString("masa_R");
            laporan = intent.getExtras().getString("laporan");
        }
        else {
            Toast.makeText(getApplicationContext(),"Kosong..",Toast.LENGTH_LONG).show();
        }

        img = (ImageView) findViewById(R.id.gambar_reklame_info);
        Picasso.with(this).load(gambar).into(img);


        txtPercentage = (TextView) findViewById(R.id.txtPercentage1);
        progressBar2 = (ProgressBar) findViewById(R.id.progressBar2);

        b_updatefoto = (Button) findViewById(R.id.b_updatefoto);

        if (laporan != null){
            text_set();
        }

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    public void text_set (){
        TextView id_teks = (TextView)findViewById(R.id.textViewid);
        TextView no_for = (TextView)findViewById(R.id.textViewnoF);
        TextView teks_rek = (TextView)findViewById(R.id.textViewteks);
        TextView alamat_rek = (TextView)findViewById(R.id.textViewalamat);
        TextView detail_rek = (TextView)findViewById(R.id.textViewdetail);
        TextView sipr_R = (TextView)findViewById(R.id.textViewsipr);
        TextView nama_P = (TextView)findViewById(R.id.textViewnama);
        TextView jenis_R = (TextView)findViewById(R.id.textViewjenis);
        TextView ukuran_r = (TextView)findViewById(R.id.textViewukuran);
        TextView masa_ber = (TextView)findViewById(R.id.textViewmasa);
        TextView status_info = (TextView)findViewById(R.id.text_status_info);
        LinearLayout l_status = (LinearLayout)findViewById(R.id.layout_status);

        id_teks.setText(ID);
        no_for.setText(NoF);
        teks_rek.setText(TeksR);
        alamat_rek.setText(AlamatR);
        detail_rek.setText(DetailR);
        sipr_R.setText(Sipr);
        nama_P.setText(NamaP);
        jenis_R.setText(JenisR);
        ukuran_r.setText(ukuran + " m");
        masa_ber.setText(MasaBer);

        if (laporan.equals("0")){
            status = "Belum ada laporan";
            status_info.setText(status);
            l_status.setBackgroundColor(Color.RED);
        }
        else {
            status = "Sudah dilaporkan";
            status_info.setText(status);
            l_status.setBackgroundColor(Color.rgb(255, 102, 0));
        }
    }

    public void verifikasi(View view) {
        Intent a = new Intent(InfoReklame.this, FormVerifikasi.class);

        Bundle c = new Bundle();
        c.putString("id", ID);
        c.putString("nomor", NoF);
        c.putString("teks", TeksR);
        c.putString("alamat", AlamatR);
        c.putString("detail", DetailR);
        c.putString("sipr", Sipr);
        c.putString("perusahaan", NamaP);
        c.putString("jenis", JenisR);
        c.putString("ukuran", ukuran);
        c.putString("gambar", gambar);
        c.putString("waktu", MasaBer);

        a.putExtras(c);

        startActivity(a);
    }

    public void oke(View view) {
        new UploadFileToServer().execute();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }



    /**
     * Receiving activity result method will be called after closing the camera
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                // successfully captured the image
                // launching upload activity
                launchUploadActivity(true);


            } else if (resultCode == RESULT_CANCELED) {

                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();

            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }

        }
    }

    private void launchUploadActivity(boolean isImage){
        if (isImage) {
//            imgPreview.setVisibility(View.VISIBLE);
            // bimatp factory
            BitmapFactory.Options options = new BitmapFactory.Options();

            // down sizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 8;

            final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(), options);

//            fotoRek.setVisibility(View.INVISIBLE);
//            imgRek.setVisibility(View.VISIBLE);
            img.setImageBitmap(bitmap);
            b_updatefoto.setVisibility(View.VISIBLE);

//            flag = 1;
        }
    }

    /**
     * ------------ Helper Methods ----------------------
     * */

    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                Config.IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + Config.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Making progress bar visible
            progressBar2.setVisibility(View.VISIBLE);

            // updating progress bar value


            // updating percentage value
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Config.FILE_UPLOAD_URL);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 500));
                            }
                        });

                File sourceFile = new File(fileUri.getPath());

                // Adding file data to http body
                entity.addPart("image", new FileBody(sourceFile));

                // Extra parameters if you want to pass to server
                entity.addPart("website",
                        new StringBody("www.ve.info"));
                entity.addPart("email", new StringBody(ID));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e(TAG, "Response from server: " + result);

            // showing the server response in an alert dialog
            showAlert(result);
//            txtSukses.setVisibility(View.VISIBLE);
//            txtSukses.setText("SUKSES");
            b_updatefoto.setVisibility(View.INVISIBLE);
            progressBar2.setVisibility(View.INVISIBLE);

            super.onPostExecute(result);
        }

    }

    private void showAlert(String message) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage(message).setTitle("Response from Servers")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // do nothing
                    }
                });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    public void lokasi(View view){
        Intent intent = new Intent(this, MapsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_update, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.update:
                if (Lati != 0) {
                    new AlertDialog.Builder(this)
                            .setTitle("UPDATE")
                            .setMessage("Apakah anda yakin untuk menyimpan lokasi untuk Reklame ini?")
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(InfoReklame.this, String.valueOf(Lati) + "\n" + "\n" + String.valueOf(Longi), Toast.LENGTH_LONG).show();
                                    new Thread(){
                                        public void run(){
                                            update();
                                        }
                                    }.start();
                                }
                            })
                            .setNegativeButton("Tidak", null)
                            .show();
                }
                else {
                    Toast.makeText(InfoReklame.this, "Koordinat tidak di update", Toast.LENGTH_LONG).show();
                }
                return true;
            case android.R.id.home:
                //Write your logic here
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void update()
    {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        nameValuePairs.add(new BasicNameValuePair("id",ID));
        nameValuePairs.add(new BasicNameValuePair("latitude",String.valueOf(Lati)));
        nameValuePairs.add(new BasicNameValuePair("longitude",String.valueOf(Longi)));

        try
        {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Config.UPDATE_KOORDINAT);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 1", e.toString());

        }

        try
        {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success : " + result);
        }
        catch(Exception e)
        {
            Log.e("Fail 2", e.toString());
        }

        Log.e("Result", result);
        try
        {

        }
        catch(Exception e)
        {
            Log.e("Fail 3", e.toString());
        }
    }


}
