package adita.verifikasi;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import adita.verifikasi.Config.Config;

public class RegisterActivity extends AppCompatActivity {
    EditText box_nama, box_nomor, box_email, box_password;
    String nama, nomor, email, password;
    InputStream is=null;
    String result=null;
    String line=null;
    int flag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        inisial();
    }

    public void inisial(){
        box_nama = (EditText) findViewById(R.id.input_name);
        box_nomor = (EditText) findViewById(R.id.input_nomor);
        box_email = (EditText) findViewById(R.id.input_email);
        box_password = (EditText) findViewById(R.id.input_password);
    }

    public void ambilisi(){
        nama = box_nama.getText().toString();
        nomor = box_nomor.getText().toString();
        email = box_email.getText().toString();
        password = box_password.getText().toString();

        if (nama.length() > 1 && nomor.length() > 1 && email.length() > 1 && password.length() > 1){
            new Thread(){
                public void run(){
                    update();
                }
            }.start();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 1 second
                            if (flag == 1){
                                Toast.makeText(RegisterActivity.this, nama.length(), Toast.LENGTH_LONG).show();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                //Do something after 1 second
                                                pindah();
                                            }
                                        }, 2000);
                                    }
                                });
                                flag = 0;
                            }
                        }
                    }, 2000);
                }
            });
        }

        else {
            Toast.makeText(RegisterActivity.this, "Silahkan isi setiap kolom", Toast.LENGTH_LONG).show();
        }

    }

    public void pindah(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void create(View view){
        ambilisi();
    }

    public void update()
    {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        nameValuePairs.add(new BasicNameValuePair("nama",nama));
        nameValuePairs.add(new BasicNameValuePair("nomor_pegawai",nomor));
        nameValuePairs.add(new BasicNameValuePair("email",email));
        nameValuePairs.add(new BasicNameValuePair("password",password));

        try
        {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Config.SIGNUP);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
            flag = 1;
        }
        catch(Exception e)
        {
            Log.e("Fail 1", e.toString());

        }

        try
        {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success : " + result);
        }
        catch(Exception e)
        {
            Log.e("Fail 2", e.toString());
        }

        Log.e("Result", result);
        try
        {

        }
        catch(Exception e)
        {
            Log.e("Fail 3", e.toString());
        }
    }
}
